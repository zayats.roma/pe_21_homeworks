const burgerBtn = document.getElementsByClassName("burger-btn")[0];
const menuList = document.getElementsByClassName("header__menu-list")[0];

burgerBtn.addEventListener('click', () => {
    burgerBtn.classList.toggle("burger-btn--toggle");
    menuList.classList.toggle("header__menu-list--mobile");
});
const gulp = require('gulp'),
    clean = require('gulp-clean'),
    sass = require('gulp-sass'),
    cleanCSS = require('gulp-clean-css'),
    concat = require('gulp-concat'),
    postcss = require("gulp-postcss"),
    autoprefixer = require('autoprefixer'),
    minify = require('gulp-js-minify'),
    imagemin = require('gulp-imagemin'),
    browserSync = require('browser-sync');

const paths = {
    src: {
        styles: 'src/scss/**/*.scss',
        js: 'src/js/**/*.js',
        img: 'src/img/**/*'
    },

    dist: {
        build: "dist/",
        styles: 'dist/css',
        js: 'dist/js',
        img: 'dist/img'
    }
};

const cleanDist = () => (
    gulp.src(paths.dist.build, {allowEmpty: true})
        .pipe(clean())
);

const buildSCSS = () => (
    gulp.src(paths.src.styles)
        .pipe(concat('style.min.css'))
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss([autoprefixer()]))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest(paths.dist.styles))
);

const buildJS = () => (
    gulp.src(paths.src.js)
        .pipe(concat('script.min.js'))
        .pipe(minify())
        .pipe(gulp.dest(paths.dist.js))
);

const imgBuild = () => (
    gulp.src(paths.src.img)
        .pipe(imagemin())
        .pipe(gulp.dest(paths.dist.img))
);

const runDev = () => {
    browserSync.init({
        server: {
            baseDir: "../hw_2"
        }
    });
    gulp.watch(paths.src.styles, buildSCSS).on('change', browserSync.reload);
    gulp.watch(paths.src.js, buildJS).on('change', browserSync.reload)
};

gulp.task('clean', cleanDist);
gulp.task('buildSCSS', buildSCSS);
gulp.task('buildJS', buildJS);

gulp.task('build', gulp.series(
    cleanDist,
    buildSCSS,
    buildJS,
    imgBuild
));

gulp.task('dev', gulp.series(
    cleanDist,
    buildSCSS,
    buildJS,
    imgBuild,
    runDev
));
fetch("https://swapi.dev/api/films/")
  .then(response => response.json())
  .then(data => data.results.forEach(episode => {
    const episodeBlock = document.createElement("div");
    const episodeTitle = document.createElement("p");
    const episodeNumber = document.createElement("p");
    const episodePrehistory = document.createElement("p");
    const episodeCharactersList = document.createElement("ul");

    episodeBlock.classList.add("episode-block");
    episodeTitle.classList.add("episode-title");
    episodePrehistory.classList.add("episode-prehistory");

    episodeTitle.append(episode.title);
    episodeNumber.append(`EPISODE: ${episode.episode_id}`);
    episodePrehistory.append(`PREHISTORY: ${episode.opening_crawl}`);

    episodeBlock.append(episodeTitle, episodeNumber, episodePrehistory);
    document.body.append(episodeBlock);

    let requests = episode.characters.map(item => fetch(item));
    Promise.all(requests)
      .then(responses => Promise.all(responses.map(item => item.json())))
      .then(heroes => heroes.forEach(hero => {
        const episodeCharactersListItem = document.createElement("li");
        episodeCharactersListItem.append(hero.name)
        episodeCharactersList.append(episodeCharactersListItem)
      }))
    episodeBlock.append(episodeCharactersList);
  }))
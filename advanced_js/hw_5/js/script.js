const ipifyURL = "https://api.ipify.org/?format=json";
const ipInfoBtn = document.getElementById("ip-info-btn");

async function identifyUser() {
  const response = await fetch(ipifyURL);
  const json = await response.json();
  return json.ip
}

async function identifyAddress() {
  const response = await fetch("http://ip-api.com/json/" + await identifyUser() + "?fields=continent,country,regionName,city,district");
  return await response.json()
}

ipInfoBtn.addEventListener("click", async function() {
  let {continent, country, regionName, city, district} = await identifyAddress();

  const ipInfoContinent = document.getElementById("ip-info-continent");
  const ipInfoCountry = document.getElementById("ip-info-country");
  const ipInfoRegionName = document.getElementById("ip-info-region-name");
  const ipInfoCity = document.getElementById("ip-info-city");
  const ipInfoDistrict = document.getElementById("ip-info-district");
  ipInfoContinent.textContent = `Continent: ${continent}`;
  ipInfoCountry.textContent = `Country: ${country}`;
  ipInfoRegionName.textContent = `Region name: ${regionName}`;
  ipInfoCity.textContent = `City: ${city}`;

  if (district === "") {
    district = "n/a"
  }
  ipInfoDistrict.textContent = `District: ${district}`;
})
class Employee {
  constructor(options) {
    this.name = options.name
    this.age = options.age
    this.salary = options.salary
  }

  set name(value) {
    if (!isNaN(value)) {
      alert("Name value must be a string");
      return
    }
    this._name = value;
  }
  get name() {
    return this._name+"-Employee";
  }

  set age(value) {
    if (isNaN(value) || value < 18 || value > 100) {
      alert("Enter the correct age");
      return
    }
    this._age = value;
  }
  get age() {
    return this._age+"-years";
  }

  set salary(value) {
    if (isNaN(value)) {
      alert("Salary value must be numerical");
      return
    }
    this._salary = value;
  }
  get salary() {
    return this._salary+"-USD";
  }
}

class Programmer extends Employee {
  constructor(options) {
    super(options)
    this.lang = options.lang
  }

  set salary(value) {
    if (isNaN(value)) {
      alert("Salary value must be numerical");
      return
    }
    this._salary = value;
  }
  get salary() {
    return this._salary*3+"-USD";
  }
}

const feDeveloper = new Programmer({
  name: "feDeveloper",
  age: 25,
  lang: "javaScript",
  salary: 1700,
})

const javaDeveloper = new Programmer({
  name: "javaDeveloper",
  age: 26,
  lang: "Java",
  salary: 1900,
})

const netDeveloper = new Programmer({
  name: "netDeveloper",
  age: 27,
  lang: ".NET",
  salary: 1800,
})

console.log(feDeveloper);
console.log(javaDeveloper);
console.log(netDeveloper);
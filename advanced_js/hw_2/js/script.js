const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const rootBox = document.getElementById("root");
const booksList = document.createElement("ul");

function checkBook (object) {
    const {author, name, price} = object;
    if (author && name && price) {
        const booksListItem = document.createElement("li");
        booksListItem.append(author + " - ", name + " - ", price);
        booksList.append(booksListItem);
    } else if (!author) {
        throw new Error(`${name} - Author is undefined`)
    } else if (!name) {
        throw new Error(`${author} - Name is undefined`)
    } else if (!price) {
        throw new Error(`${author} - ${name} - Price is undefined`)
    }
}

books.forEach(el => {
    try {
        checkBook(el);
    } catch (error) {
        console.error(error.message);
    }
});

rootBox.append(booksList);
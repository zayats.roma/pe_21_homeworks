let changeThemeBtn = document.createElement("button");
changeThemeBtn.classList.add("change-theme-btn");
changeThemeBtn.innerText = "Change theme";
document.body.append(changeThemeBtn);

let cssLink = document.getElementsByTagName("link")[1];

window.onload = function () {
    if (localStorage.getItem("cssFile")) {
        let pageTheme = localStorage.getItem("cssFile");
        cssLink.setAttribute("href", pageTheme);
    }

    changeThemeBtn.addEventListener("click", () => {
        if (cssLink.getAttribute("href") === "css/style-1.css") {
            cssLink.setAttribute("href", "css/style-2.css");
            localStorage.setItem("cssFile", "css/style-2.css");
        } else if (cssLink.getAttribute("href") === "css/style-2.css") {
            cssLink.setAttribute("href", "css/style-1.css");
            localStorage.setItem("cssFile", "css/style-1.css");
        }
    })
}
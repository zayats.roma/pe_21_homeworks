$(".tabs-content li:first").show();

$(".tabs-title").on("click",function () {
    $(".tabs-title")
        .removeClass("active")
        .eq($(this).index()).addClass("active");
    $(".tabs-content li")
        .hide()
        .eq($(this).index()).show();
});
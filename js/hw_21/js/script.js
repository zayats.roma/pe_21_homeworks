const createCircleBtn = document.getElementsByClassName("create-circle-btn")[0];
const circleDiameterInput = document.createElement("input");
circleDiameterInput.classList.add("circle-diameter-input");
circleDiameterInput.setAttribute("type","number");
circleDiameterInput.setAttribute("value","50");
const circleDiameterInputCaption = document.createElement("span");
circleDiameterInputCaption.classList.add("circle-diameter-input-caption");
circleDiameterInputCaption.innerText = "diameter (10-65)";
const approveCreationBtn = document.createElement("button");
approveCreationBtn.classList.add("approve-creation-btn");
approveCreationBtn.innerText = "Create!";
const circlesContainer = document.createElement("div");
circlesContainer.classList.add("circles-container");

createCircleBtn.addEventListener("click", () => {
    createCircleBtn.after(circleDiameterInput);
    createCircleBtn.after(circleDiameterInputCaption);

    circleDiameterInput.addEventListener("mouseout" || "focusout", () => {
        if (circleDiameterInput.value >= 10 && circleDiameterInput.value <= 65 && circleDiameterInput.value !== "") {
            circleDiameterInput.after(approveCreationBtn);
        } else {
            approveCreationBtn.remove();
        }
    });
});

approveCreationBtn.addEventListener("click", () => {
    approveCreationBtn.after(circlesContainer);
    createCircleBtn.remove();
    circleDiameterInput.remove();
    circleDiameterInputCaption.remove();
    approveCreationBtn.remove();
    circlesCreator();
});

document.addEventListener("click", event => {
    if (event.target.className === "circle-element") {
        event.target.remove();
    }
});

function circlesCreator() {
    for (let i = 0; i < 100; i++) {
        let circleElement = document.createElement("div");
        circleElement.setAttribute("class", "circle-element");
        circleElement.setAttribute("style", getDiameter());
        circleElement.style.backgroundColor = getColor();
        circlesContainer.append(circleElement);
    }

    function getDiameter() {
        return `padding: ${circleDiameterInput.value/2}px`;
    }

    function getColor () {
        return `rgb(${Math.floor(Math.random()*255)},${Math.floor(Math.random()*255)},${Math.floor(Math.random()*255)})`;
    }
}

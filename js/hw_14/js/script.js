$(document).ready(function () {
    let $navMenu = $(".nav-menu").clone(true)
        .removeClass("nav-menu")
        .addClass("anchor-nav-menu")
        .prependTo($(".news-block"));

    let $anchorLinksArr = Array.from($navMenu[0].children);
    $anchorLinksArr.forEach(el => {
        el.firstChild.setAttribute("href", "#" + el.textContent.split(" ")[0]);
    });

    $("ul li a").on("click", function (event) {
        event.preventDefault();
        const href = $(this).attr("href");

        const offset = $(href).offset().top;
        $("html,body").animate({
            scrollTop: offset,
        }, 1000)
    });

    const $toTopBtn = $(".to-top-btn");
    $(window).on("scroll", function () {
        ($(window).scrollTop() >= innerHeight) ? $toTopBtn.fadeIn() : $toTopBtn.fadeOut();
    });
    $toTopBtn.on("click", function () {
        $("html,body").animate({
            scrollTop : 0,
        }, 1000)
    });

    $(".toggle-btn").click(function () {
        $(".popular-posts-block").slideToggle("slow");
    });
});
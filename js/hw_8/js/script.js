const fragment = document.createDocumentFragment();

const priceDiv = document.createElement("div");
priceDiv.classList.add("price-div");
priceDiv.innerText = "Price, USD:";

const priceInput = document.createElement("input");
priceInput.classList.add("price-input");
priceInput.placeholder = "Enter price";

const messageSpan = document.createElement("span");
messageSpan.classList.add("message-span");

const messageButton = document.createElement("a");
messageButton.classList.add("message-button");

const errorSpan = document.createElement("span");
errorSpan.classList.add("error-span");

fragment.append(priceDiv,priceInput);
document.body.append(fragment);

function priceChecker(userPrice) {
    if (userPrice > 0 && userPrice < 10000) {
        messageSpan.innerText = `Actual price: $${userPrice}`;
        priceInput.style.backgroundColor = "lightgreen";

        errorSpan.remove();
        priceInput.style.borderColor = "dimgrey";

        fragment.append(messageSpan, messageButton);
        document.body.append(fragment);
    } else if (userPrice === "") {
        priceInput.style.borderColor = "dimgrey";
    } else {
        errorSpan.innerText = "Incorrect data used!";
        priceInput.style.borderColor = "red";

        messageSpan.remove();
        messageButton.remove();
        priceInput.style.backgroundColor = "transparent";

        fragment.append(errorSpan);
        document.body.append(fragment);
    }
}

priceInput.addEventListener("focus", () => {
    priceInput.style.borderColor = "limegreen";
});

priceInput.addEventListener("focusout", () => {
    priceChecker(priceInput.value);
});

messageButton.addEventListener("click", () => {
    messageSpan.remove();
    messageButton.remove();
    priceInput.value = "";
    priceInput.style.backgroundColor = "transparent";
    messageButton.style.fontWeight = "normal";
    messageButton.style.borderColor = "lightgrey";
})

messageButton.addEventListener("mouseover", () => {
    messageButton.style.fontWeight = "bold";
    messageButton.style.borderColor = "dimgrey";
});

messageButton.addEventListener("mouseleave", () => {
    messageButton.style.fontWeight = "normal";
    messageButton.style.borderColor = "lightgrey";
});

// SWITCH

// function priceChecker (userPrice) {
//     switch (userPrice > 0 && userPrice < 10000) {
//         case true: function fTrue() {
//                         messageSpan.innerText = `Actual price: $${userPrice}`;
//                         priceInput.style.backgroundColor = "lightgreen";
//
//                         errorSpan.remove();
//                         priceInput.style.borderColor = "black";
//
//                         fragment.append(messageSpan, messageButton);
//                         document.body.append(fragment);
//                     }
//                     fTrue(); break;
//
//         case false: function fFalse() {
//                         errorSpan.innerText = "Incorrect data used!";
//                         priceInput.style.borderColor = "red";
//
//                         messageSpan.remove();
//                         messageButton.remove();
//                         priceInput.style.backgroundColor = "transparent";
//
//                         fragment.append(errorSpan);
//                         document.body.append(fragment);
//                     }
//                     fFalse(); break;
//
//         default: messageSpan.innerText = "Something went wrong";
//     }
// }

// GOT TEXT CONTENT OF TAB BUTTONS==================================================
const tabsArr = [...document.getElementsByClassName('tabs-title')];
let tabsTitlesArr = [];
for (let i = 0; i < tabsArr.length; i++) {
    tabsTitlesArr.push(tabsArr[i].innerText.toLowerCase());
}
// console.log(tabsTitlesArr);


// ADDED SPECIFIC CLASS-NAMES TO TAB BUTTONS========================================
for (let i = 0; i < tabsTitlesArr.length; i++) {
    tabsArr[i].classList.add(tabsTitlesArr[i]);
}
// console.log(tabsArr);


// ADDED SPECIFIC CLASS-NAMES TO CONTENT PARAGRAPHS=================================
const tabsContentsArr = [...document.getElementsByClassName("tabs-content")[0].children];
for (let i = 0; i < tabsContentsArr.length; i++) {
    tabsContentsArr[i].classList.add(tabsTitlesArr[i]);
}
// console.log(tabsContentsArr);


// =================================================================================
let tabsUls = document.querySelectorAll("ul")[0];
// console.log(tabsUls);


// DISPLAYED DEFAULT ACTIVE-TAB's CONTENT===========================================
const activeTab = tabsUls.getElementsByClassName('active')[0];
// const activeTab = tabsArr.find(i => i.className.includes("active"));

for (let i = 0; i < tabsContentsArr.length; i++) {
    if (activeTab.classList.contains(tabsContentsArr[i].className)) {
        tabsContentsArr[i].hidden = false;
    } else {
        tabsContentsArr[i].hidden = true;
    }
}


// DISPLAYED ANY ACTIVE-TAB's CONTENT===============================================
tabsUls.addEventListener("click", function (event) {
        tabsUls.getElementsByClassName('active')[0].classList.remove('active');
        event.target.classList.add("active");

        const targetTab = event.target;
        for (let i = 0; i < tabsContentsArr.length; i++) {
            if (targetTab.classList.contains(tabsContentsArr[i].className)) {
                tabsContentsArr[i].hidden = false;
            } else {
                tabsContentsArr[i].hidden = true;
            }
        }
});
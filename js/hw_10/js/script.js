const passForm = document.getElementsByClassName("password-form")[0];
// console.log(passForm);
const passFormLabels = [...document.getElementsByClassName("input-wrapper")];
// console.log(passFormLabels);
const passFormInputs = [...passForm.getElementsByTagName("input")];
// console.log(passFormInputs);
const passFormInputsIcons = [...passForm.getElementsByClassName("icon-password")];
// console.log(passFormInputsIcons);
const passFormSubmit = passForm.getElementsByTagName("button")[0];
// console.log(passFormSubmit);
const passErrorMessage = document.createElement("span");
passErrorMessage.classList.add("pass-error-message");
passErrorMessage.innerText = "Please, use similar passwords!";

passFormInputsIcons.forEach(element => {
    element.addEventListener("click", (event) => {
        if (event.target.classList.contains("fa-eye")) {
            event.target.classList.replace("fa-eye", "fa-eye-slash");
            event.target.previousElementSibling.setAttribute("type", "password");

        }
        else if (event.target.classList.contains("fa-eye-slash")) {
            event.target.classList.replace("fa-eye-slash", "fa-eye");
            event.target.previousElementSibling.setAttribute("type", "text");
        }
        })
    })

passFormSubmit.addEventListener("click", (event) => {
    if (passFormInputs[0].value === passFormInputs[1].value) {
        alert ("You are welcome!");
    } else {
        event.preventDefault();
        passFormLabels[1].after(passErrorMessage);
        passFormInputs[1].addEventListener("focusout", () => {
            passErrorMessage.remove();
        })
    }
})
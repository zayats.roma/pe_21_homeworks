function slideShow (i) {
    let slideChanger = setInterval(function nextSlide() {
        let j = i + 1;
        for (i; i < j; i++) {
            if (j < imgArr.length) {
                imgArr[i].classList.add("hidden-img");
                imgArr[j].classList.remove("hidden-img");
            } else if (j === imgArr.length) {
                imgArr[i].classList.add("hidden-img");
                imgArr[0].classList.remove("hidden-img");
                slideShow(0);
            }
        }
    }, millisecondsToDisplay);

    function stoppedImgIndexFinder() {
        for (let i = 0; i < imgArr.length; i++) {
            if (imgArr[i].className === "image-to-show") {
                stoppedImgIndex = i;
                return stoppedImgIndex;
            }
        }
    }

    stopBtn.addEventListener("click", () => {
        stopBtn.remove();
        imgContainer.append(resumeBtn);
        clearInterval(slideChanger);
        imgArr.findIndex(stoppedImgIndexFinder);
    })

    resumeBtn.addEventListener("click", () => {
        resumeBtn.remove();
        imgContainer.append(stopBtn);
        slideShow(stoppedImgIndex);
    });
}

const imgArr = [...document.getElementsByClassName("image-to-show")];
const imgContainer = document.getElementsByClassName("images-wrapper")[0];
const stopBtn = document.createElement("button");
stopBtn.classList.add("stop-btn");
stopBtn.innerText = "stop";
imgContainer.append(stopBtn);
const resumeBtn = document.createElement("button");
resumeBtn.classList.add("resume-btn");
resumeBtn.innerText = "resume";
let stoppedImgIndex;
let millisecondsToDisplay;

millisecondsToDisplay = 10000;
slideShow(0);
const buttonsArr = [...document.getElementsByClassName("btn")];

document.addEventListener("keyup", event => {
    buttonsArr.forEach(element => {
        if (event.key.toUpperCase() === element.textContent.toUpperCase()) {
            element.classList.add("keyup-event-btn");
        } else {
            element.classList.remove("keyup-event-btn");
        }
    });
});

	// for (let i = 0; i < buttonsArr.length; i++) {
    	//     document.addEventListener("keyup", event => {
   	//         if (event.key.toUpperCase() === buttonsArr[i].textContent.toUpperCase()) {
    	//             buttonsArr[i].classList.add("keyup-event-btn");
    	//         } else {
    	//             buttonsArr[i].classList.remove("keyup-event-btn");
    	//         }
    	//     })
    	// }
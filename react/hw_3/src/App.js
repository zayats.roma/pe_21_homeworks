import React, {useEffect, useState} from 'react';
import './App.scss';
import AddToCartModal from "./components/AddToCartModal/AddToCartModal";
import RemoveFromCartModal from "./components/RemoveFromCartModal/RemoveFromCartModal";
import Header from "./components/Header/Header";
import AppRoutes from "./routes/AppRoutes";

const App = () => {

  const [openModal, setOpenModal] = useState(null)
  const [productsList, setProductsList] = useState([])
  const [currentTargetProps, setCurrentTargetProps] = useState({})

  const displayModal = (modalTitle, props) => {
    setOpenModal(modalTitle)
    setCurrentTargetProps(props)
  }

  const closeModal = () => {
    setOpenModal(null)
  }

  useEffect(() => {
    getProducts()
  }, [])

  const getProducts = () => {
    fetch("productsList.json")
      .then(res => res.json())
      .then(data => {
        // console.log(data);
        setProductsList(data)
      })
  }

  const addToCart = () => {
    if (openModal === "cartModal") {
      return <AddToCartModal
        currentTargetProps={currentTargetProps}
        closeModal={closeModal}/>
    }
  }

  const removeFromCart = () => {
    if (openModal === "removeModal") {
      return <RemoveFromCartModal
        currentTargetProps={currentTargetProps}
        closeModal={closeModal}/>
    }
  }

    return (
      <div className="App">
        <Header/>
        <AppRoutes
          productsList={productsList}
          displayModal={displayModal}
          closeModal={closeModal}
        />
        {addToCart()}
        {removeFromCart()}
      </div>
    );
}

export default App;
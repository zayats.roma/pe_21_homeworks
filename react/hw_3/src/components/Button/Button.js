import React from 'react';
import "./Button.scss"
import PropTypes from "prop-types"

const Button = (props) => {
    const {className, text, backgroundColor, onClick} = props
    return (
      <button className={className}
              style={{backgroundColor: backgroundColor}}
              onClick={onClick}
      >{text}</button>
    );
}

Button.propTypes = {
  className: PropTypes.string.isRequired,
  text: PropTypes.string,
  backgroundColor: PropTypes.string,
  onClick: PropTypes.func.isRequired
}

Button.defaultProps = {
  text: "Button",
  backgroundColor: "orangered"
}

export default Button;
import React, {useEffect, useState} from 'react';
import "./ProductItem.scss"
import Button from "../Button/Button";
import PropTypes from "prop-types";
import CloseItemBtn from "../CloseItemBtn/CloseItemBtn";
import {favIconUnpicked, favIconPicked} from "../IconsLinks/IconsLinks"

const ProductItem = (props) => {
  const [inFavorites, setInFavorites] = useState(false)

  useEffect(() => {
    const {vendorCode} = props
    const localFavoritesArr = JSON.parse(localStorage.getItem("favorites")) || [];
    const isFavoriteBool = localFavoritesArr.some(fav => fav.vendorCode === vendorCode);
    setInFavorites(isFavoriteBool)
  }, [props])

  const addToFavorites = (itemProps) => {
    let storageFavorites = localStorage.getItem("favorites");
    if (!storageFavorites) {
      localStorage.setItem("favorites", JSON.stringify([itemProps]));
    }
    if (storageFavorites) {
      if (storageFavorites.includes(JSON.stringify(itemProps))) {
        let parsedStorageFavorites = JSON.parse(storageFavorites)
        let updatedStorageFavorites = parsedStorageFavorites.filter(e => e.vendorCode !== itemProps.vendorCode);
        localStorage.setItem("favorites", JSON.stringify(updatedStorageFavorites))
        props.displayModal("removeFav", props)
      } else {
        let updatedStorageFavorites = JSON.parse(localStorage.getItem("favorites"));
        updatedStorageFavorites.push(itemProps);
        localStorage.setItem("favorites", JSON.stringify(updatedStorageFavorites));
      }
    }
  }

  const {className, title, price, imageUrl, displayModal, inCart} = props;
  return (
    <div className={className}>

      {inCart && <CloseItemBtn
        className="btn btn__item--delete"
        text="+"
        onClick={() => {
          displayModal("removeModal", props)
        }}
      />}

      <div className="product-item__title">
        <h4>{title}</h4>
        <h5>${price}</h5>
      </div>
      <div>
        <img className="product-item-img" src={imageUrl} alt={`${title}`}/>
      </div>

      {!inCart && <div className="product-item__buttons-container">
        <Button
          className="btn btn__add-to-cart"
          text="add to cart"
          backgroundColor="yellowgreen"
          onClick={() => {
            displayModal("cartModal", props, inFavorites)
          }}
        />
        <img
          className="img__add-to-favorites"
          alt="add-to-favorites"
          src={inFavorites ? favIconPicked
            : favIconUnpicked}
          onClick={() => {
            setInFavorites(!inFavorites)
            addToFavorites(props)
          }}
        />
      </div>}

      {inCart && <div className="product-item__buttons-container">
        <img
          className="img__add-to-favorites"
          alt="add-to-favorites"
          src={inFavorites ? favIconPicked
            : favIconUnpicked}
          onClick={() => {
            setInFavorites(!inFavorites)
            addToFavorites(props)
          }}
        />
      </div>}
    </div>
  );
}

ProductItem.propTypes = {
  className: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  imageUrl: PropTypes.string.isRequired,
  displayModal: PropTypes.func.isRequired,
  vendorCode: PropTypes.number.isRequired,
  color: PropTypes.string
}

ProductItem.defaultProps = {
  color: "no info"
}

export default ProductItem;
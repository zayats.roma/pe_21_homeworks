import React, {Component} from 'react';
import './App.scss';

import Button from './components/Button/Button';
import ModalOne from "./components/ModalOne/ModalOne";
import ModalTwo from "./components/ModalTwo/ModalTwo";

class App extends Component {
  state = {
    openModal: null
  }

  closeModal = () => {
    this.setState({openModal: "null"})
  }

  changeState = (modalNum) => {
    this.setState({openModal: modalNum})
  }

  render() {
    return (
      <div className="App">

        <header className="App__navbar">
          <Button
            className={"btn btn__create-modal"}
            text={"open first modal"}
            backgroundColor="yellowgreen"
            onClick={() => {
              this.changeState("first")
            }}
          />

          <Button
            className={"btn btn__create-modal"}
            text={"open second modal"}
            backgroundColor="dodgerblue"
            onClick={() => {
              this.changeState("second")
            }}
          />
        </header>

        {this.state.openModal === "first" && <ModalOne closeModal={this.closeModal}/>}
        {this.state.openModal === "second" && <ModalTwo closeModal={this.closeModal}/>}
      </div>
    );
  }
}

export default App;
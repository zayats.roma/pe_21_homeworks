import React, {Component} from 'react';
import Button from "../Button/Button";
import "./ModalTemplate.scss";

class ModalTemplate extends Component {
  render() {
    let {className, header, text, closeButton, actions} = this.props;
    return (
      <div className="modal__container"
           onClick={(e) => {
             if (e.target === e.currentTarget) {
               this.props.closeModal()
             }
           }}
      >
        <div className={className}>
          <div className="modal__close-btn-container">
            {closeButton && <Button
                                  className={"btn btn__close-modal"}
                                  text={"+"}
                                  backgroundColor="whitesmoke"
                                  onClick={() => {
                                    this.props.closeModal()
                                  }}
                               />
            }
          </div>
          <h2>{header}</h2>
          <p>{text}</p>
          <div>{actions}</div>
        </div>
      </div>
    );
  }
}

export default ModalTemplate;
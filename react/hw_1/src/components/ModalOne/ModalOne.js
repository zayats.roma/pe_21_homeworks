import React from 'react';
import ModalTemplate from "../ModalTemplate/ModalTemplate"
import Button from "../Button/Button";

class ModalOne extends ModalTemplate {
  render() {
    return (
      <ModalTemplate className={"modal"}
                     header={"Please confirm this suggestion:"}
                     text={"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."}
                     closeButton={true}
                     closeModal={this.props.closeModal}

                     actions={[
                       <Button className={"btn btn__manipulate-modal"}
                               text={"Agree"}
                               backgroundColor="steelblue"
                               key={Math.random()}
                               onClick={() => {
                                 console.log("action one button")
                               }}
                       />,

                       <Button className={"btn btn__manipulate-modal"}
                               text={"Not agree"}
                               backgroundColor="orangered"
                               key={Math.random()}
                               onClick={() => {
                                 console.log("action two button")
                               }}
                       />
                     ]}
      />
    );
  }
}

export default ModalOne;
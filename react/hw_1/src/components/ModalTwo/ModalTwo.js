import React from 'react';
import ModalTemplate from "../ModalTemplate/ModalTemplate"
import Button from "../Button/Button";

class ModalTwo extends ModalTemplate {
  render() {
    return (
      <ModalTemplate className={"modal"}
                     header={"Do you want to close this modal window ASAP?"}
                     text={"I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness..."}
                     closeButton={true}
                     closeModal={this.props.closeModal}

                     actions={[
                       <Button className={"btn btn__manipulate-modal"}
                               text={"Yes"}
                               backgroundColor="lightsteelblue"
                               key={Math.random()}
                               onClick={() => {
                                 console.log("action one button")
                               }}
                       />,

                       <Button className={"btn btn__manipulate-modal"}
                               text={"No"}
                               backgroundColor="coral"
                               key={Math.random()}
                               onClick={() => {
                                 console.log("action two button")
                               }}
                       />
                     ]}
      />
    );
  }
}

export default ModalTwo;
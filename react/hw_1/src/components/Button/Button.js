import React, {Component} from 'react';
import "./Button.scss"

class Button extends Component {
  render() {
    const {className, text, backgroundColor, onClick} = this.props
    return (
      <button className={className}
              style={{backgroundColor: backgroundColor}}
              onClick={onClick}
      >{text}</button>
    );
  }
}

export default Button;
import React, {Component} from 'react';
import "./ProductItem.scss"
import Button from "../Button/Button";
import PropTypes from "prop-types";

class ProductItem extends Component {

  state = {
    inFavorites: false
  }

  addToFavorites(itemProps) {
    let storageFavorites = localStorage.getItem("favorites");

    if (!storageFavorites) {
      localStorage.setItem("favorites", JSON.stringify([itemProps]));
    }

    if (storageFavorites) {
      if (storageFavorites.includes(JSON.stringify(itemProps))) {
        let parsedStorageFavorites = JSON.parse(storageFavorites)
        let updatedStorageFavorites = parsedStorageFavorites.filter(e => e.vendorCode !== itemProps.vendorCode);
        localStorage.setItem("favorites", JSON.stringify(updatedStorageFavorites))
      } else {
        let updatedStorageFavorites = JSON.parse(localStorage.getItem("favorites"));
        updatedStorageFavorites.push(itemProps);
        localStorage.setItem("favorites", JSON.stringify(updatedStorageFavorites));
      }
    }
  }

  render() {
    const {className, title, price, imageUrl, displayModal} = this.props
    // console.log(this.props.color);
    return (
      <div className={className}>
        <div className="product-item__title">
          <h4>{title}</h4>
          <h5>${price}</h5>
        </div>
        <div>
          <img className="product-item-img" src={imageUrl} alt={`${title}`}/>
        </div>
        <div className="product-item__buttons-container">
          <Button
            className="btn btn__add-to-cart"
            text="add to cart"
            backgroundColor="yellowgreen"
            onClick={() => {
              displayModal("cartModal", this.props)
            }}
          />
          <img
            className="img__add-to-favorites"
            alt="add-to-favorites"
            src={this.state.inFavorites ? "https://d29fhpw069ctt2.cloudfront.net/icon/image/49761/preview.svg"
              : "https://d29fhpw069ctt2.cloudfront.net/icon/image/38027/preview.svg"}
            onClick={() => {
              this.setState({inFavorites: !this.state.inFavorites})
              this.addToFavorites(this.props)
            }}
          />
        </div>
      </div>
    );
  }
}

ProductItem.propTypes = {
  className: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  imageUrl: PropTypes.string.isRequired,
  displayModal: PropTypes.func.isRequired,
  vendorCode: PropTypes.number.isRequired,
  color: PropTypes.string
}

ProductItem.defaultProps = {
  color: "no info"
}

export default ProductItem;
import React, {Component} from 'react';
import "./ProductList.scss"
import ProductItem from "../ProductItem/ProductItem";
import PropTypes from "prop-types";

class ProductsList extends Component {

  render() {
    const {productsList, displayModal} = this.props;

    return (
      <div className="products-list">
        {productsList.map(el => {
          return <ProductItem className="product-item__container"
                              title={el.title}
                              price={el.price}
                              imageUrl={el.imageUrl}
                              key={el.vendorCode}
                              vendorCode={el.vendorCode}
                              color={el.color}
                              displayModal={displayModal}
          />}
        )}
      </div>
    );
  }
}

ProductsList.propTypes = {
  productsList: PropTypes.arrayOf(PropTypes.object).isRequired,
  displayModal: PropTypes.func.isRequired
}

export default ProductsList;
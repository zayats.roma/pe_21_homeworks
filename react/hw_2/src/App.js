import React, {Component} from 'react';
import './App.scss';

import Button from './components/Button/Button';
import AddToCartModal from "./components/AddToCartModal/AddToCartModal";
import ProductsList from "./components/ProductsList/ProductsList";

class App extends Component {

  state = {
    openModal: null,
    productsList: [],
    currentTargetProps: {}
  }

  displayModal = (modalTitle, props) => {
    this.setState({openModal: modalTitle, currentTargetProps: props})
  }

  closeModal = () => {
    this.setState({openModal: "null"})
  }

  componentDidMount() {
    this.getProducts()
  }

  getProducts() {
    fetch("productsList.json")
      .then(res => res.json())
      .then(data => this.setState({productsList: data}))
  }

  render() {

    return (
      <div className="App">

        <header className="App__navbar">
          <div>
            <a className="App__logo-text" href="#">tastyShop</a>
          </div>
          <div>
            <Button
              className="btn btn__show-cart"
              text="Cart"
              backgroundColor="yellowgreen"
              onClick={() => {

              }}
            />

            <Button
              className="btn btn__show-favorites"
              text="Favorites"
              backgroundColor="dodgerblue"
              onClick={() => {

              }}
            />
          </div>
        </header>

        <ProductsList
          productsList={this.state.productsList}
          displayModal={this.displayModal}
        />

        {this.state.openModal === "cartModal" && <AddToCartModal
                                                    currentTargetProps={this.state.currentTargetProps}
                                                    closeModal={this.closeModal}/>}

      </div>
    );
  }
}

export default App;
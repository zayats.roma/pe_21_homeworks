import initialStore from "../initialStore";
import {CHECKOUT_FORM_SUBMITTING} from "./checkoutFormActions";

export default function checkoutFormReducer(checkoutFormInitialValues = initialStore.checkoutFormValues, {type, payload}) {
    switch (type) {
        case CHECKOUT_FORM_SUBMITTING:
            localStorage.setItem("cart", JSON.stringify([]));
            console.log("Local storage Cart is cleared");
            return {
                ...checkoutFormInitialValues,
                ...payload,
            }
        default:
            return checkoutFormInitialValues
    }
}
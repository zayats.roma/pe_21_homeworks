export const CHECKOUT_FORM_SUBMITTING = "CHECKOUT_FORM_FULFILLMENT";

export function checkoutFormData(values) {
    return (dispatch) => {
        dispatch({type: CHECKOUT_FORM_SUBMITTING, payload: values})
    }
}
import {combineReducers} from "redux";
import serverReducer from "../server/serverReducer";
import {modalTypeReducer, modalTargetReducer} from "../modal/modalReducer";
import checkoutFormReducer from "../checkoutForm/checkoutFormReducer";

export default combineReducers({
    productsList: serverReducer,
    openModal: modalTypeReducer,
    currentTargetProps: modalTargetReducer,
    checkoutFormValues: checkoutFormReducer
})
const initialStore = {
    productsList: [],
    openModal: null,
    currentTargetProps: {},
    checkoutFormValues: {
        firstName: "",
        secondName: "",
        age: "",
        address: "",
        phone: "",
        termsConditions: false
    }
};

export default initialStore;
import {setModalType, setCurrentTargetProps} from "./modalActions";

describe("modal actions tests", () => {
    test("setModalType fn() test", () => {
        const modalName = "testModal";
        const mockSetModalType = jest.fn((testTitle) => setModalType(testTitle));
        mockSetModalType(modalName);

        expect(mockSetModalType).toHaveReturned();
        expect(mockSetModalType).toHaveBeenCalled();
        expect(mockSetModalType).toHaveBeenCalledWith(modalName);
    });

    test("setCurrentTargetProps fn() test", () => {
        const payloadProps = {title: "any title", price: 100500};
        const mocksetCurrentTargetProps = jest.fn((testProps) => setCurrentTargetProps(testProps));
        mocksetCurrentTargetProps(payloadProps);

        expect(mocksetCurrentTargetProps).toHaveReturned();
        expect(mocksetCurrentTargetProps).toHaveBeenCalled();
        expect(mocksetCurrentTargetProps).toHaveBeenCalledWith(payloadProps);
    });
});
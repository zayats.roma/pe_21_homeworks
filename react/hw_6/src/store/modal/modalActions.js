export const MODAL_TYPE = "MODAL_TYPE";
export const MODAL_TARGET = "MODAL_TARGET";

export function setModalType(modalName) {
    return (dispatch) => {
        dispatch({type: MODAL_TYPE, payload: modalName})
    }
}

export function setCurrentTargetProps(props) {
    return (dispatch) => {
        dispatch({type: MODAL_TARGET, payload: props})
    }
}
import {modalTypeReducer, modalTargetReducer} from "./modalReducer"
import {MODAL_TYPE, MODAL_TARGET} from "./modalActions";

describe("modal reducer test", () => {

    test("set modal type", () => {
        const initialStoreOpenModal = null;
        const emptyAction = {type: MODAL_TYPE}
        const testAction = {
            type: MODAL_TYPE,
            payload: "test modal"
        }
        const fakeAction = {type: "NO_SUCH_ACTION", payload: "no such modal"}

        expect(modalTypeReducer(initialStoreOpenModal, emptyAction)).toBeUndefined();
        expect(modalTypeReducer(initialStoreOpenModal, testAction)).toBe(testAction.payload);
        expect(modalTypeReducer(initialStoreOpenModal, fakeAction)).toBeNull();
        expect(modalTypeReducer(initialStoreOpenModal, fakeAction)).toStrictEqual(null);
        expect(modalTypeReducer(initialStoreOpenModal, fakeAction)).toBeFalsy();
        expect(modalTypeReducer(initialStoreOpenModal, fakeAction)).not.toBeNaN();
    });

    test("set current target in modal", () => {
        const initialStoreCurrentTargetProps = {};
        const testPayload = {title: "testTitle", price: 999, vendorCode: 1e10};
        const emptyAction = {type: MODAL_TARGET}
        const testAction = {
            type: MODAL_TARGET,
            payload: testPayload
        }
        const fakeAction = {
            type: "NO_SUCH_ACTION",
            payload: testPayload
        }

        expect(modalTargetReducer(initialStoreCurrentTargetProps, emptyAction)).toBeUndefined();
        expect(modalTargetReducer(initialStoreCurrentTargetProps, testAction)).toBe(testAction.payload);
        expect(modalTargetReducer(initialStoreCurrentTargetProps, fakeAction)).toEqual({});
        expect(modalTargetReducer(initialStoreCurrentTargetProps, fakeAction)).toBeTruthy();
        expect(modalTargetReducer(initialStoreCurrentTargetProps, fakeAction)).not.toContain(null);
        expect(modalTargetReducer(initialStoreCurrentTargetProps, fakeAction)).not.toContain(undefined);
        expect(modalTargetReducer(initialStoreCurrentTargetProps, fakeAction)).not.toBeNaN();
    });
});
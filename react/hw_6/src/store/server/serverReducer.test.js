import serverReducer from "./serverReducer";
import {LOAD_PRODUCTS_LIST} from "./serverActions";

describe("server reducer test", () => {

    test("GET items from server", () => {

        // const getProducts = fetch("productsList.json")
        //     .then(res => res.json())
        //     .then(data => {
        //         return ({type: LOAD_PRODUCTS_LIST, payload: data})
        //     })

        const initialStoreProductsList = [];
        const testPayloadProductsList = [
            {title: "testTitle", price: 100500, color: "testy-color"},
            {vendorCode: 9e9, imageUrl: "", title: "noTitle"}
        ];

        const emptyAction = {type: LOAD_PRODUCTS_LIST, payload: [..."empty-payload"]}
        const testAction = {
            type: LOAD_PRODUCTS_LIST,
            payload: testPayloadProductsList
        }
        const fakeAction = {type: "NO_SUCH_ACTION", payload: [..."no-items-action"]}

        expect(serverReducer(initialStoreProductsList, emptyAction)).not.toStrictEqual(initialStoreProductsList);
        expect(serverReducer(initialStoreProductsList, testAction)).toEqual(expect.arrayContaining(testPayloadProductsList));
        expect(serverReducer(initialStoreProductsList, fakeAction)).toBe(initialStoreProductsList);
    });
});
import React from 'react';
import ModalTemplate from "../ModalTemplate/ModalTemplate"
import Button from "../Button/Button";
import {v4 as uuidv4} from 'uuid';
import PropTypes from "prop-types";

const RemoveFromCartModal = ({closeModal, currentTargetProps}) => {

  const deleteFromCart = (itemProps) => {
    let storageCart = JSON.parse(localStorage.getItem("cart"));
    let updatedStorageCart = storageCart.filter(e => e.vendorCode !== itemProps.vendorCode)
    localStorage.setItem("cart", JSON.stringify(updatedStorageCart))
  }

  return (
    <ModalTemplate className="modal"
                   header="Remove this from your Cart?"
                   text=""
                   closeButton={true}
                   closeModal={closeModal}
                   currentTargetProps={currentTargetProps}

                   actions={[
                     <Button className="btn btn__modal--manipulate"
                             text="Ok"
                             backgroundColor="dodgerblue"
                             key={uuidv4()}
                             onClick={() => {
                               deleteFromCart(currentTargetProps)
                               closeModal()
                             }}
                     />,

                     <Button className="btn btn__modal--manipulate"
                             text="Cancel"
                             key={uuidv4()}
                             onClick={() => {
                               closeModal()
                             }}
                     />
                   ]}
    />
  );
}

RemoveFromCartModal.propTypes = {
  closeModal: PropTypes.func.isRequired,
  currentTargetProps: PropTypes.object.isRequired
}

export default RemoveFromCartModal;
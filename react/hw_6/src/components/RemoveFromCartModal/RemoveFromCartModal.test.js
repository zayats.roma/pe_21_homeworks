import React from "react";
import {render} from "@testing-library/react";
import '@testing-library/jest-dom';
import RemoveFromCartModal from "./RemoveFromCartModal";

describe("remove from cart modal tests", () => {
    test("rendering", () => {
        const removeFromCartModal = render(<RemoveFromCartModal closeModal={() => {}} currentTargetProps={{
            className: "testClassName",
            title: "test ChocolateBar"
        }}/>);

        expect(removeFromCartModal).toBeDefined();
    });
});
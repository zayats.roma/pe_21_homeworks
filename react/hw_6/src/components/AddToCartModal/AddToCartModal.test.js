import React from "react";
import {render} from "@testing-library/react";
import AddToCartModal from "./AddToCartModal";
import '@testing-library/jest-dom';

describe("add to cart modal tests", () => {
    test("rendering", () => {
        const addToCarModal = render(<AddToCartModal closeModal={() => {}} currentTargetProps={{
            className: "testClassName",
            title: "test ChocolateBar"
        }}/>);

        expect(addToCarModal).toBeDefined();
    });
});
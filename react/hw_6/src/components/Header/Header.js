import React from 'react';
import {Link} from "react-router-dom";
import Button from "../Button/Button";
import "./Header.scss"

const Header = () => (
      <header data-testid="header-node" className="App__navbar">
        <div>
          <Link data-testid="shopName" className="App__logo-text" to="/">tastyShop</Link>
        </div>
        <div>
          <Link to="/cart">
            <Button
              className="btn btn__show-cart"
              text="Cart"
              backgroundColor="yellowgreen"
              onClick={() => {
              }}
            />
          </Link>

          <Link to="/favorites">
            <Button
              className="btn btn__show-favorites"
              text="Favorites"
              backgroundColor="dodgerblue"
              onClick={() => {
              }}
            />
          </Link>
        </div>
      </header>
    );

export default Header;
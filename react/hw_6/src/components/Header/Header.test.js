// import React from "react";
// import {render, unmountComponentAtNode} from "react-dom";
// import Header from "./Header";
// import {act} from "@testing-library/react";
// import {BrowserRouter} from "react-router-dom";

// let container = null;

// beforeEach(() => {
//     container = document.createElement("div");
//     document.body.append(container);
// });

// afterEach(() => {
//     unmountComponentAtNode(container);
//     container.remove();
//     container = null;
// });

// describe("header component tests", () => {
//     test("rendering", () => {
//         act(() => {
//             render(
//                 <BrowserRouter>
//                     <Header/>
//                 </BrowserRouter>,
//                 container);
//         });

//         const header = container.getElementsByTagName("header")[0];
//         const SHOPNAME_REGEX = /tastyShop/;

//         expect(header).toBeDefined()
//         expect(SHOPNAME_REGEX.test(header.textContent)).toBeTruthy()
//     });
// });

import React from "react";
import {render} from "@testing-library/react";
import Header from "./Header";
import {BrowserRouter} from "react-router-dom";

describe("header component tests", () => {
    test("rendering", () => {
        const header = render(
                <BrowserRouter>
                    <Header/>
                </BrowserRouter>);

        expect(header).toBeDefined();
        expect(header.getByTestId("shopName").textContent).toBe("tastyShop");
    });
});
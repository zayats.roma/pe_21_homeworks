import React from "react";
import {render} from "@testing-library/react";
import Button from "./Button";

describe("button tests", () => {
    test("rendering", () => {
        const {getByTestId} = render(<Button onClick={() => {
        }}/>);
        const btnTestTemplate = getByTestId("btnTestTemplate");
        const defaultBtnClassName = "btn";
        expect(btnTestTemplate).toBeDefined();
        expect(btnTestTemplate.className).toBe(defaultBtnClassName);
    });
});
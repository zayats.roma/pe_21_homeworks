import React from 'react';
import {ErrorMessage, Field, Form, Formik} from "formik";
import Card from "@material-ui/core/Card";
import {CardContent, TextField} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import {boolean, number, object, string} from "yup";
import FormGroup from "@material-ui/core/FormGroup";
import Button from "../Button/Button";
import {connect} from "react-redux";
import {checkoutFormData} from "../../store/checkoutForm/checkoutFormActions";

const CheckoutForm = ({checkoutFormValues, checkoutFormData}) => {
    return (
        <Card>
            <CardContent>
                <Typography variant="h4">Checkout form</Typography>
                <Formik validationSchema={
                    object({
                        firstName: string().required("It is a required field!").min(2).max(20),
                        secondName: string().required("It is a required field!").min(2).max(20),
                        age: number().required("It is a required field!").min(18).max(100),
                        address: string().required("It is a required field!").min(20).max(50),
                        phone: number().required("It is a required field!"),
                        termsConditions: boolean().oneOf([true])
                    })
                }
                        initialValues={checkoutFormValues}
                        onSubmit={(values) => {
                            setTimeout(() => {
                                console.log("Clients information:", values);
                                console.log("Purchased items", JSON.parse(localStorage.getItem("cart")).map(e => e.vendorCode));
                                checkoutFormData(values)
                            }, 3000)
                        }}>
                    {({values, errors, isSubmitting, isValidating}) => (
                        <Form>
                            <FormGroup>
                                <Field name="firstName" as={TextField} label="First name"/>
                                <ErrorMessage name="firstName"/>

                                <Field name="secondName" as={TextField} label="Second name"/>
                                <ErrorMessage name="secondName"/>

                                <Field name="age" type="number" as={TextField} label="Age"/>
                                <ErrorMessage name="age"/>

                                <Field name="address" as={TextField} label="Address" multiline rows={2}/>
                                <ErrorMessage name="address"/>

                                <Field name="phone" as={TextField} label="Phone number"/>
                                <ErrorMessage name="phone"/>
                            </FormGroup>

                            <FormControlLabel name="termsConditions"
                                              control={<Checkbox />}
                                              onChange={() => {
                                                  values.termsConditions = !values.termsConditions;
                                              }}
                                              label={"I accept the terms and conditions"}/>
                            <ErrorMessage name="termsConditions"/>

                            <Button type="submit"
                                    disabled={isSubmitting || isValidating}
                                    className="btn btn__checkout--proceed"
                                    text="Checkout"
                                    backgroundColor="yellowgreen"
                                    onClick={() => {
                                    }}/>
                        </Form>
                    )}
                </Formik>
            </CardContent>
        </Card>
    )
};

const mapStoreToProps = (store) => {
    // console.log("Initial and modified store", store.checkoutFormValues);
    return {
        checkoutFormValues: store.checkoutFormValues
    }
}

const mapDispatchToProps = (dispatch) => ({
    checkoutFormData: (values) => dispatch(checkoutFormData(values))
})

export default connect(mapStoreToProps, mapDispatchToProps)(CheckoutForm);
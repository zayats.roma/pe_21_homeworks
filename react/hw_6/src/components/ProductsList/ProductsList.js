import React from 'react';
import "./ProductList.scss"
import ProductItem from "../ProductItem/ProductItem";
import PropTypes from "prop-types";
import {v4 as uuidv4} from 'uuid';


const ProductsList = (props) => {
  const {itemClassName: className, productsList, displayModal, closeModal, inCart} = props;

  return (
    <div className="products-list">
      {productsList.map(el => {
          return <ProductItem className={className}
                              title={el.title}
                              price={el.price}
                              imageUrl={el.imageUrl}
                              key={uuidv4()}
                              vendorCode={el.vendorCode}
                              color={el.color}
                              displayModal={displayModal}
                              closeModal={closeModal}
                              inCart={inCart}
          />})}
    </div>
  );
}

ProductsList.propTypes = {
  productsList: PropTypes.arrayOf(PropTypes.object).isRequired,
  displayModal: PropTypes.func.isRequired
}

export default ProductsList;
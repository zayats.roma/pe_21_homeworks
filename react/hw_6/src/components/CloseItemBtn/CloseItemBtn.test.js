import React from "react";
import {render} from "@testing-library/react";
import CloseItemBtn from "./CloseItemBtn";

describe("closeItem button tests", () => {
    test("rendering", () => {
        const closeItemBtn = render(<CloseItemBtn onClick={() => {}}/>);

        expect(closeItemBtn).toBeDefined();
    });
});
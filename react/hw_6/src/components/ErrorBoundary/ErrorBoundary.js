import React, {useState} from 'react'

const ErrorBoundary = (props) => {
  let [hasError, setHasError] = useState(false)

  const goHome = () => {
    setHasError(false);
  }

  const {children} = props;
  if (hasError) {
    return (
      <div>
        <h1>An error has occurred</h1>
        <div>
          <button onClick={goHome}>Go to home page</button>
        </div>
      </div>
    )
  }
  return children;
}

export default ErrorBoundary
import React from 'react';
import "./ModalTemplate.scss";
import PropTypes from "prop-types";
import CloseItemBtn from "../CloseItemBtn/CloseItemBtn";

const ModalTemplate = (props) => {

  const {className, header, closeButton, actions, closeModal, const: {title, imageUrl, price} = props.currentTargetProps} = props;

  const closeCurrentModal = (e) => {
    if (e.target === e.currentTarget) {
      props.closeModal()
    }
  }

  return (
    <div className="modal__container"
         onClick={(e) => closeCurrentModal(e)}
    >
      <div className={className}>
        <div className="modal__btn-close-container">
          {closeButton && <CloseItemBtn
            className="btn btn__modal--close"
            closeModal={closeModal}
            text="+"
            onClick={() => {
              closeModal()
            }}
          />}
        </div>
        <h2>{header}</h2>

        <div className="modal__product-box">
          <div className="modal__product-box--img-box">
            <img className="modal__product-box--img" alt={`${title}`}
                 src={imageUrl}/>
          </div>
          <div className="modal__product-box--text-box">
            <p>{title}</p>
            <p>${price}</p>
          </div>
        </div>

        <div>{actions}</div>
      </div>
    </div>
  );
}

ModalTemplate.propTypes = {
  className: PropTypes.string.isRequired,
  header: PropTypes.string.isRequired,
  closeButton: PropTypes.bool.isRequired,
  actions: PropTypes.arrayOf(PropTypes.object).isRequired,
  closeModal: PropTypes.func.isRequired,
  currentTargetProps: PropTypes.object.isRequired
}

export default ModalTemplate;
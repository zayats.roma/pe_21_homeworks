import React, {useState} from 'react';
import ProductsList from "../components/ProductsList/ProductsList";
import Button from "../components/Button/Button";
import CheckoutForm from "../components/CheckoutForm/CheckoutForm";

const CartPage = ({displayModal, closeModal}) => {

    const localStorageCart = JSON.parse(localStorage.getItem("cart"));
    const [checkoutForm, setCheckoutForm] = useState(true);

    if (localStorageCart.length >= 1 && checkoutForm) {
        return (
            <div>
                <ProductsList
                    itemClassName="product-item__cart-container"
                    productsList={localStorageCart}
                    displayModal={displayModal}
                    closeModal={closeModal}
                    inCart={true}
                />
                <Button className="btn btn__checkout--proceed"
                        text="Proceed"
                        backgroundColor="yellowgreen"
                        onClick={() => {
                            setCheckoutForm(false)
                        }}/>
            </div>
        );
    } else if (localStorageCart.length >= 1 && !checkoutForm) {
        return <CheckoutForm/>
    } else {
        return (
            <div className="no-items-msg">
                <p>No items here yet...</p>
            </div>
        )
    }
}

export default CartPage;
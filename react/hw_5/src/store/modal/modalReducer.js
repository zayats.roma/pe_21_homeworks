import initialStore from "../initialStore";
import {MODAL_TYPE, MODAL_TARGET} from "./modalActions";

export function modalTypeReducer(modalTypeFromStore = initialStore.openModal, {type, payload}) {
    switch (type) {
        case MODAL_TYPE:
            return payload
        default:
            return modalTypeFromStore
    }
}

export function modalTargetReducer(modalTargetFromStore = initialStore.currentTargetProps, {type, payload}) {
    switch (type) {
        case MODAL_TARGET:
            return payload
        default:
            return modalTargetFromStore
    }
}
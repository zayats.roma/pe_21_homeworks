const initialStore = {
 productsList: [],
 openModal: null,
 currentTargetProps: {}
};

export default initialStore;
import {applyMiddleware, createStore} from "redux";
import {composeWithDevTools} from "redux-devtools-extension";
import initialStore from "./initialStore";
import thunk from "redux-thunk";
import rootReducer from "./reducers/rootReducer";

// import {compose} from "redux";
// const store = createStore(
//     rootReducer,
//     initialStore,
//     compose(
//         applyMiddleware(thunk),
//         window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
//     )
// );

const store = createStore(
    rootReducer,
    initialStore,
    composeWithDevTools(
        applyMiddleware(thunk)
    )
);


export default store;
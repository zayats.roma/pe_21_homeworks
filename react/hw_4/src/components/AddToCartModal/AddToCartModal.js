import React from 'react';
import ModalTemplate from "../ModalTemplate/ModalTemplate"
import Button from "../Button/Button";
import {v4 as uuidv4} from 'uuid';
import PropTypes from "prop-types";

const AddToCartModal = (props) => {

  const addToCart = (itemProps) => {
    let storageCart = localStorage.getItem("cart");
    if (!storageCart) {
      localStorage.setItem("cart", JSON.stringify([itemProps]));
    } else {
      let updatedStorageCart = JSON.parse(localStorage.getItem("cart"));
      updatedStorageCart.push(itemProps);
      localStorage.setItem("cart", JSON.stringify(updatedStorageCart));
    }
  }

  const {closeModal, currentTargetProps} = props
  return (
    <ModalTemplate className="modal"
                   header="Put this to your Cart?"
                   closeButton={true}
                   closeModal={closeModal}
                   currentTargetProps={currentTargetProps}

                   actions={[
                     <Button className="btn btn__modal--manipulate"
                             text="Ok"
                             backgroundColor="dodgerblue"
                             key={uuidv4()}
                             onClick={() => {
                               addToCart(currentTargetProps)
                               closeModal()
                             }}
                     />,

                     <Button className="btn btn__modal--manipulate"
                             text="Cancel"
                             key={uuidv4()}
                             onClick={() => {
                               closeModal()
                             }}
                     />
                   ]}
    />
  );
}

AddToCartModal.propTypes = {
  closeModal: PropTypes.func.isRequired,
  currentTargetProps: PropTypes.object.isRequired
}

export default AddToCartModal;
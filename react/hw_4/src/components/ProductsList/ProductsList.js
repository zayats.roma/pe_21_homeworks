import React from 'react';
import "./ProductList.scss"
import ProductItem from "../ProductItem/ProductItem";
import PropTypes from "prop-types";

const ProductsList = (props) => {
  const {itemClassName: className, productsList, displayModal, closeModal, inCart} = props;

  return (
    <div className="products-list">
      {productsList.map(el => {
          return <ProductItem className={className}
                              title={el.title}
                              price={el.price}
                              imageUrl={el.imageUrl}
                              key={el.vendorCode}
                              vendorCode={el.vendorCode}
                              color={el.color}
                              displayModal={displayModal}
                              closeModal={closeModal}
                              inCart={inCart}
          />})}
    </div>
  );
}

ProductsList.propTypes = {
  productsList: PropTypes.arrayOf(PropTypes.object).isRequired,
  displayModal: PropTypes.func.isRequired
}

export default ProductsList;
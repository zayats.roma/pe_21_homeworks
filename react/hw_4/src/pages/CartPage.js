import React from 'react';
import ProductsList from "../components/ProductsList/ProductsList";

const CartPage = ({displayModal, closeModal}) => {

  const localStorageCart = JSON.parse(localStorage.getItem("cart"));

  if (localStorageCart.length > 0) {
    return (
        <div>
          <ProductsList
              itemClassName="product-item__cart-container"
              productsList={localStorageCart}
              displayModal={displayModal}
              closeModal={closeModal}
              inCart={true}
          />
        </div>
    );
  } else {
    return (
        <div className="no-items-msg">
          <p>No items here yet...</p>
        </div>
    )
  }
}

export default CartPage;
import React from 'react';
import ProductsList from "../components/ProductsList/ProductsList";

const FavoritesPage = ({displayModal}) => {

  const localStorageFavorites = JSON.parse(localStorage.getItem("favorites"));

  if (localStorageFavorites.length > 0) {
    return (
        <ProductsList
            itemClassName="product-item__container"
            productsList={localStorageFavorites}
            displayModal={displayModal}
            inCart={false}
        />
    );
  } else {
    return (
        <div className="no-items-msg">
          <p>No items here yet...</p>
        </div>
    )
  }
}

export default FavoritesPage;